package com.wallet.dtos;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class WalletItemDTO {

	
	private Long id;
	@NotNull(message = "Insira o id da carteira")
	private Long wallet;
	@NotNull(message = "Informe uma data")
	@JsonFormat(shape=JsonFormat.Shape.STRING,pattern = "dd-MM-YYYY",locale = "pt-BR",timezone = "Brazil/East")
	private Date date;
	@NotNull(message = "Informe um tipo")
	@Pattern(regexp = "^(ENTRADA|SAÍDA)$",message="Para o tipo somente são aceitos os valores ENTRADA ou SAÍDA")
	private String type;
	@NotNull(message = "Informe uma descricao")
	@Length(min=5,message="A descrção deve ter no minimo 5 caracteres")
	private String description;
	@NotNull(message = "informe um valor")
	private BigDecimal value;
}
