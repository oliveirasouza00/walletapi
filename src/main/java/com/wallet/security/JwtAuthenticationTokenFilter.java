package com.wallet.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.wallet.security.utils.JwtTokenUtil;

public class JwtAuthenticationTokenFilter  extends OncePerRequestFilter{

	
	private static final String AUT_HEADER = "Authorization";
	private static final String BEARER_PREFIX = "Bearer";
	
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	
	
@Override
protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException{
			String token = request.getHeader(AUT_HEADER);
			if (token != null && token.startsWith(BEARER_PREFIX)) {
				token = token.substring(7);
			}
			
			String username = jwtTokenUtil.getUsernameFromToken(token);
			
			if (username != null && SecurityContextHolder.getContext().getAuthentication()==null) {
				
				UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
				if (jwtTokenUtil.validTokens(token)) {
					UsernamePasswordAuthenticationToken authetication = new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
					authetication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(authetication);
								
				}
				
			}

			chain.doFilter(request, response);
	
	}
	
	
	
	
}
