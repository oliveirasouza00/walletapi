package com.wallet.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wallet.entity.WalletItem;
import com.wallet.utils.enums.TypeEnum;

public interface WalletItemRepository  extends JpaRepository<WalletItem, Long>{

	Page<WalletItem> findAllByWalletIdAndDateGreaterThanEqualAndDateLessThanEqual(Long wallet, Date init,Date end,Pageable page);
	List<WalletItem> findByWalletIdAndType(Long wallet , TypeEnum type);
	
	/*
	 * Utilizando JPQL
	 */
	@Query(value = "select sum(value) from WalletItem wi Where wi.wallet.id= :wallet")
	BigDecimal sumByWalletId(@Param("wallet")Long wallet);
	
}
